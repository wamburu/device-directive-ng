# Changelog

## 0.2.5 (3 November, 2014)

* Bump to lelylan-ng 0.2.2

## 0.2.0 (14 August, 2014)

* Uses new [lelylan client](http://lelylan.github.io/lelylan-ng/)
* Uses new [oauth directive](http://andreareginato.github.io/oauth-ng/)
* New [documentation site](http://lelylan.github.com/device-directive-ng)
* Code refactoring
* Dynamic template
* New CSS style
